package in.eightfolds.easysqlite.commons.db;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import in.eightfolds.easysqlite.commons.db.annotation.ClientId;
import in.eightfolds.easysqlite.commons.db.annotation.Id;
import in.eightfolds.easysqlite.commons.db.annotation.Transient;
import in.eightfolds.easysqlite.commons.db.bean.CommonsEntity;
import in.eightfolds.easysqlite.commons.db.bean.ExtraPropertyData;
import in.eightfolds.easysqlite.commons.db.criteria.Criteria;
import in.eightfolds.easysqlite.commons.db.criteria.Criterion;
import in.eightfolds.easysqlite.commons.db.criteria.Junction;
import in.eightfolds.easysqlite.commons.db.criteria.LogicalExpression;
import in.eightfolds.easysqlite.commons.db.criteria.Order;
import in.eightfolds.easysqlite.commons.db.criteria.PropertyExpression;
import in.eightfolds.easysqlite.commons.db.exception.DaoException;
import in.eightfolds.easysqlite.commons.util.Collections;
import in.eightfolds.easysqlite.commons.util.Reflection;
import in.eightfolds.easysqlite.commons.util.Strings;

public class DBUtil {

    private static DBUtil instance;

    private DBUtil() {
    }

    public static DBUtil getInstance(Context context) {
        if (instance == null) {
            instance = new DBUtil();
        }
        return instance;
    }

    /**
     * Add data into table
     *
     * @param context
     * @param object
     * @return newly inserted row id.
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     */
    public long add(Context context, Object object, Class<?> clazz)
            throws IllegalAccessException, IllegalArgumentException {
        String entityName = CommonsEntity.getEntityName(clazz);
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();

        return db.insert(entityName, null, getContentValues(object));
    }

    public CommonsEntity select(Context context, Class<?> clss,
                                Criteria criteria, long id) throws SQLException,
            InstantiationException, IllegalAccessException,
            InvocationTargetException, NoSuchMethodException,
            NoSuchFieldException {
        String entityName = CommonsEntity.getEntityName(clss);
        String idColumnName = CommonsEntity.getIdColumnName(clss);
        List<String> columnNameList = CommonsEntity.getColumnNames(clss, false,
                false);
        List<String> selectionArgs = new ArrayList<String>();

        Cursor cursor = getCursor(context, entityName, selectionArgs,
                columnNameList, idColumnName, criteria, clss, id);

        if (cursor.moveToNext()) {
            return (CommonsEntity) setValueToObject(cursor, columnNameList,
                    clss);
        }
        return null;

    }

    public List<? extends CommonsEntity> select(Context context, Class<?> clss,
                                                SQLiteQueryBuilder queryBuilder, List<String> columnNameList,
                                                String selection) throws InstantiationException,
            IllegalAccessException, InvocationTargetException,
            NoSuchMethodException, NoSuchFieldException {
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        String[] projection = columnNameList.toArray(new String[columnNameList
                .size()]);
        Cursor cursor = queryBuilder.query(db, projection, selection, null,
                null, null, null);
        List<CommonsEntity> commonsEntities = new ArrayList<CommonsEntity>();
        columnNameList = CommonsEntity.getColumnNames(clss, false, false);
        while (cursor.moveToNext()) {
            commonsEntities.add((CommonsEntity) setValueToObject(cursor,
                    columnNameList, clss));
        }

        return commonsEntities;
    }

    private Cursor getCursor(Context context, String entityName,
                             List<String> selectionArgs, List<String> columnNameList,
                             String idColumnName, Criteria criteria, Class<?> clss, long id) throws SQLException {
        String where = idColumnName + " = ?";
        if (criteria != null) {
            where = toCriteriaString(clss, criteria);
            if (Strings.isEmpty(where)) {
                where = "1=1";
            }
        }

        int i = 0;
        if (criteria == null) {
            selectionArgs.add(i, "" + id);
        } else {
            populateCriteriaValues(selectionArgs, clss, criteria, i);
        }

        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();

        return db
                .query(entityName, columnNameList
                                .toArray(new String[columnNameList.size()]),
                        !selectionArgs.isEmpty() ? where : null, selectionArgs
                                .toArray(new String[selectionArgs.size()]),
                        (selectionArgs.isEmpty()
                                && criteria.getGroupBys() != null && !criteria
                                .getGroupBys().isEmpty()) ? where : null, null, null);
    }

    public List<? extends CommonsEntity> selectAll(Context context,
                                                   Class<?> clss, Criteria criteria, long id)
            throws SQLException, InstantiationException,
            IllegalAccessException, InvocationTargetException,
            NoSuchMethodException, NoSuchFieldException {
        String entityName = CommonsEntity.getEntityName(clss);
        String idColumnName = CommonsEntity.getIdColumnName(clss);
        List<String> columnNameList = CommonsEntity.getColumnNames(clss, false,
                false);
        List<String> selectionArgs = new ArrayList<String>();

        Cursor cursor = getCursor(context, entityName, selectionArgs,
                columnNameList, idColumnName, criteria, clss, id);
        List<CommonsEntity> commonsEntities = new ArrayList<CommonsEntity>();

        while (cursor.moveToNext()) {
            commonsEntities.add((CommonsEntity) setValueToObject(cursor,
                    columnNameList, clss));
        }

        return commonsEntities;
    }

    public List<? extends Object> selectAllId(Context context, Class<?> clss,
                                              Criteria criteria) throws SQLException {
        String entityName = CommonsEntity.getEntityName(clss);
        String idColumnName = CommonsEntity.getIdColumnName(clss);
        List<String> selectionArgs = new ArrayList<String>();

        String where = null;
        if (criteria != null) {
            where = toCriteriaString(clss, criteria);
            if (Strings.isEmpty(where)) {
                where = "1=1";
            }
            populateCriteriaValues(selectionArgs, clss, criteria, 0);
        }

        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        Cursor cursor = db.query(entityName, new String[]{idColumnName},
                where, selectionArgs.toArray(new String[selectionArgs.size()]),
                null, null, null);
        List<Long> idList = new ArrayList<Long>();
        while (cursor.moveToNext()) {
            idList.add(cursor.getLong(cursor.getColumnIndex(idColumnName)));
        }
        return idList;
    }

    public Object setValueToObject(Cursor cursor, List<String> columnNameList,
                                   Class<?> clss) throws InstantiationException,
            IllegalAccessException, InvocationTargetException,
            NoSuchMethodException, NoSuchFieldException {
        Object commonsEntityObject = Reflection.getNewInstance(clss);
        for (String columnName : columnNameList) {
            Reflection
                    .setProperty(
                            commonsEntityObject,
                            columnName,
                            getValueFromCursor(commonsEntityObject, columnName,
                                    cursor));
        }
        return commonsEntityObject;
    }

    private Object getValueFromCursor(Object commonsEntityObject,
                                      String columnName, Cursor cursor) throws IllegalAccessException,
            InvocationTargetException, NoSuchMethodException,
            NoSuchFieldException {
        Class<?> propertyType = Reflection.getPropertyType(commonsEntityObject,
                columnName);
        if (propertyType == null) {
            return null;
        }
        if (propertyType.equals(String.class)) {
            return cursor.getString(cursor.getColumnIndex(columnName));
        } else if (propertyType.equals(int.class)
                || propertyType.equals(Integer.class)) {
            return cursor.getInt(cursor.getColumnIndex(columnName));
        } else if (propertyType.equals(long.class)
                || propertyType.equals(Long.class)) {
            return cursor.getLong(cursor.getColumnIndex(columnName));
        } else if (propertyType.equals(float.class)
                || propertyType.equals(Float.class)) {
            return cursor.getFloat(cursor.getColumnIndex(columnName));
        } else if (propertyType.equals(double.class)
                || propertyType.equals(Double.class)) {
            return cursor.getDouble(cursor.getColumnIndex(columnName));
        } else if (propertyType.equals(boolean.class)
                || propertyType.equals(Boolean.class)) {
            return Boolean.parseBoolean(cursor.getString(cursor
                    .getColumnIndex(columnName)));
        } else {
            return cursor.getString(cursor.getColumnIndex(columnName));
        }
    }

    /**
     * Convenience method for updating rows in the database.
     *
     * @param context
     * @param object
     * @param whereClauseMap the optional WHERE clause to apply when updating. Passing null
     *                       will update all rows.
     * @return the number of rows affected
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     */
    public int modify(Context context, Object object,
                      Map<String, String> whereClauseMap) throws IllegalAccessException,
            IllegalArgumentException {
        String whereClause = getWhereClause(whereClauseMap);
        String[] whereArgs = getWhereArgs(whereClauseMap);
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        return db.update(getTableName(object.getClass()),
                getContentValues(object), whereClause, whereArgs);
    }

    /**
     * Convenience method for updating rows in the database.
     *
     * @param context
     * @param object
     * @param whereClause the optional WHERE clause to apply when updating. Passing null
     *                    will update all rows.
     * @return the number of rows affected
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     */
    public int modify(Context context, Object object, String whereClause)
            throws IllegalAccessException, IllegalArgumentException {
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        return db.update(getTableName(object.getClass()),
                getContentValues(object), whereClause, null);
    }

    public int modify(Context context, CommonsEntity entity, Criteria criteria,
                      Class<? extends CommonsEntity> clss) throws IllegalAccessException,
            InvocationTargetException, NoSuchMethodException, DaoException,
            NoSuchFieldException {
        return getModifySql(context, entity, clss, criteria, new String[]{});

    }

    public int getModifySql(Context context, CommonsEntity entity,
                            Class<?> clss, Criteria criteria, String... extras)
            throws IllegalAccessException, InvocationTargetException,
            NoSuchMethodException, DaoException, NoSuchFieldException {
        String entityName = CommonsEntity.getEntityName(clss);
        String idColumnName = CommonsEntity.getIdColumnName(clss);
        List<String> columnNameList = CommonsEntity.getColumnNames(clss, true,
                true);
        List<String> whereArgs = new ArrayList<String>();

        String where = idColumnName + " = ?";
        if (criteria != null) {
            where = toCriteriaString(clss, criteria);
            if (Strings.isEmpty(where)) {
                where = "1=1";
            }
        }

        preparedStatementForModify(entity, criteria, whereArgs,
                new ExtraPropertyData[]{});

        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        return db.update(entityName,
                getContentValues(clss, entity, columnNameList, extras), where,
                whereArgs.toArray(new String[whereArgs.size()]));

    }

    public void preparedStatementForModify(CommonsEntity entity,
                                           Criteria criteria, List<String> whereArgs,
                                           ExtraPropertyData... extras) throws DaoException {
        try {
            String idColumnName = entity.getIdColumnName();

            int i = 0;

            if (criteria == null) {
                Class<?> propertyType = Reflection.getPropertyType(entity,
                        idColumnName);
                Object value = Reflection.getProperty(entity, idColumnName);

                preparedStatementSetValue(whereArgs, propertyType, value, i);
            } else {
                populateCriteriaValues(whereArgs, entity.getClass(), criteria,
                        i);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new DaoException(e.toString());
        }

    }

    private void populateCriteriaValues(List<String> whereArgs, Class<?> clss,
                                        Criteria criteria, int index) throws SQLException {
        List<Criterion> criterions = criteria.getCriterions();
        if (criterions != null && criterions.size() > 0) {
            for (int i = 0; i < criterions.size(); i++) {
                Criterion criterion = criterions.get(i);
                index = populateCriterionValues(clss, criterion, whereArgs,
                        index);
            }
        }
    }

    private int populateCriterionValues(Class<?> clss, Criterion criterion,
                                        List<String> whereArgss, int index) throws SQLException {
        if (criterion instanceof LogicalExpression) {
            return populateCriterionValues(clss, (LogicalExpression) criterion,
                    whereArgss, index);
        } else if (criterion instanceof PropertyExpression) {
            return populateCriterionValues(clss,
                    (PropertyExpression) criterion, whereArgss, index);
        } else if (criterion instanceof Junction) {
            return populateCriterionValues(clss, (Junction) criterion,
                    whereArgss, index);
        }

        return -1;
    }

    private int populateCriterionValues(Class<?> clss, LogicalExpression le,
                                        List<String> whereArgs, int index) throws SQLException {
        index = populateCriterionValues(clss, le.getLhs(), whereArgs, index);
        index = populateCriterionValues(clss, le.getRhs(), whereArgs, index);
        return index;
    }

    private int populateCriterionValues(Class<?> clss, PropertyExpression pe,
                                        List<String> whereArgs, int index) throws SQLException {
        String op = pe.getOp();
        String propertyName = pe.getPropertyName();
        String otherPropertyName = pe.getOtherPropertyName();

        if (op != null) {
            if (PropertyExpression.IS_NULL.equals(op)) {
            } else if (PropertyExpression.IS_NOT_NULL.equals(op)) {
            } else if (PropertyExpression.EQUAL.equals(op)) {
                if (Strings.isEmpty(propertyName))
                    propertyName = CommonsEntity.getIdColumnName(clss);
                if (!Strings.isEmpty(otherPropertyName)) {
                } else {
                    Object value = pe.getValue1();
                    Class<?> propertyType = null;
                    if (value != null) {
                        propertyType = value.getClass();
                    }
                    preparedStatementSetValue(whereArgs, propertyType, value,
                            index);
                    index++;
                }
            } else if (PropertyExpression.NOT_EQUAL.equals(op)) {
                if (!Strings.isEmpty(otherPropertyName)) {
                } else {
                    Object value = pe.getValue1();
                    Class<?> propertyType = value.getClass();
                    preparedStatementSetValue(whereArgs, propertyType, value,
                            index);
                    index++;
                }
            } else if (PropertyExpression.BETWEEN.equals(op)) {
                Object value1 = pe.getValue1();
                Class<?> propertyType1 = value1.getClass();
                preparedStatementSetValue(whereArgs, propertyType1, value1,
                        index);
                index++;

                Object value2 = pe.getValue2();
                Class<?> propertyType2 = value2.getClass();
                preparedStatementSetValue(whereArgs, propertyType2, value2,
                        index);
                index++;
            } else if (PropertyExpression.GE.equals(op)) {
                if (!Strings.isEmpty(otherPropertyName)) {
                } else {
                    Object value = pe.getValue1();
                    Class<?> propertyType = value.getClass();
                    preparedStatementSetValue(whereArgs, propertyType, value,
                            index);
                    index++;
                }
            } else if (PropertyExpression.GT.equals(op)) {
                if (!Strings.isEmpty(otherPropertyName)) {
                } else {
                    Object value = pe.getValue1();
                    Class<?> propertyType = value.getClass();
                    preparedStatementSetValue(whereArgs, propertyType, value,
                            index);
                    index++;
                }
            } else if (PropertyExpression.LE.equals(op)) {
                if (!Strings.isEmpty(otherPropertyName)) {
                } else {
                    Object value = pe.getValue1();
                    Class<?> propertyType = value.getClass();
                    preparedStatementSetValue(whereArgs, propertyType, value,
                            index);
                    index++;
                }
            } else if (PropertyExpression.LT.equals(op)) {
                if (!Strings.isEmpty(otherPropertyName)) {
                } else {
                    Object value = pe.getValue1();
                    Class<?> propertyType = value.getClass();
                    preparedStatementSetValue(whereArgs, propertyType, value,
                            index);
                    index++;
                }
            } else if (PropertyExpression.LIKE.equals(op)) {
                Object value = pe.getValue1();
                Class<?> propertyType = value.getClass();
                preparedStatementSetValue(whereArgs, propertyType, value, index);
                index++;
            } else if (PropertyExpression.IN.equals(op)) {
                Object[] values = pe.getValues();
                for (int ii = 0; ii < values.length; ii++) {
                    Object value = values[ii];
                    Class<?> propertyType = value.getClass();
                    preparedStatementSetValue(whereArgs, propertyType, value,
                            index);
                    index++;
                }
            }
        }

        return index;
    }

    private int populateCriterionValues(Class<?> clss, Junction j,
                                        List<String> whereArgs, int index) throws SQLException {
        String op = j.getOp();
        List<Criterion> criterions = j.getCriterions();

        if (op != null) {
            if (criterions.size() > 0) {
                for (int i = 0; i < criterions.size(); i++) {
                    Criterion criterion = criterions.get(i);
                    index = populateCriterionValues(clss, criterion, whereArgs,
                            index);
                }
            }
        }

        return index;
    }

    private void preparedStatementSetValue(List<String> whereArgs,
                                           Class<?> propertyType, Object value, int index) throws SQLException {
        if (value != null) {
            if (propertyType.equals(String.class)) {
                whereArgs.add(index, value.toString());
            } else if (propertyType.equals(int.class)
                    || propertyType.equals(Integer.class)) {
                whereArgs.add(index, value.toString());
            } else if (propertyType.equals(long.class)
                    || propertyType.equals(Long.class)) {
                whereArgs.add(index, value.toString());
            } else if (propertyType.equals(float.class)
                    || propertyType.equals(Float.class)) {
                whereArgs.add(index, value.toString());
            } else if (propertyType.equals(double.class)
                    || propertyType.equals(Double.class)) {
                whereArgs.add(index, value.toString());
            } else if (propertyType.equals(boolean.class)
                    || propertyType.equals(Boolean.class)) {
                whereArgs.add(index, value.toString());
            } else if (propertyType.equals(Date.class)) {
                whereArgs.add(index, value.toString());
            } else {
                whereArgs.add(index, value.toString());
            }
        } else {
            // int type;
            // if (propertyType.equals(String.class)) {
            // type = Types.VARCHAR;
            // } else if (propertyType.equals(int.class)
            // || propertyType.equals(Integer.class)) {
            // type = Types.INTEGER;
            // } else if (propertyType.equals(long.class)
            // || propertyType.equals(Long.class)) {
            // type = Types.BIGINT;
            // } else if (propertyType.equals(float.class)
            // || propertyType.equals(Float.class)) {
            // type = Types.FLOAT;
            // } else if (propertyType.equals(double.class)
            // || propertyType.equals(Double.class)) {
            // type = Types.DOUBLE;
            // } else if (propertyType.equals(boolean.class)
            // || propertyType.equals(Boolean.class)) {
            // type = Types.BOOLEAN;
            // } else if (propertyType.equals(Date.class)) {
            // type = Types.DATE;
            // } else {
            // type = Types.OTHER;
            // }
            // ps.setNull(index, type);
            whereArgs.add(index, null);
        }
    }

    /**
     * Query the given table, returning a Cursor over the result set.
     *
     * @param context
     * @param clazz
     * @param whereClauseMap A filter declaring which rows to return, formatted as an SQL
     *                       WHERE clause (excluding the WHERE itself). Passing null will
     *                       return all rows for the given table.
     * @param projection     A list of which columns to return. Passing null will return
     *                       all columns, which is discouraged to prevent reading data from
     *                       storage that isn't going to be used.
     * @param groupBy        A filter declaring how to group rows, formatted as an SQL
     *                       GROUP BY clause (excluding the GROUP BY itself). Passing null
     *                       will cause the rows to not be grouped.
     * @param having         formatted as an SQL HAVING clause (excluding the HAVING
     *                       itself). Passing null will cause all row groups to be
     *                       included, and is required when row grouping is not being used.
     * @param sortOrder      How to order the rows, formatted as an SQL ORDER BY clause
     *                       (excluding the ORDER BY itself). Passing null will use the
     *                       default_image sort order, which may be unordered.
     * @param limit          Limits the number of rows returned by the query, formatted as
     *                       LIMIT clause. Passing null denotes no LIMIT clause.
     * @return A Cursor object, which is positioned before the first entry. Note
     * that Cursors are not synchronized, see the documentation for more
     * details.
     */
    public Cursor query(Context context, Class<?> clazz,
                        Map<String, String> whereClauseMap, String[] projection,
                        String groupBy, String having, String sortOrder, String limit) {
        String whereClause = getWhereClause(whereClauseMap);
        String[] whereArgs = getWhereArgs(whereClauseMap);
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        return db.query(getTableName(clazz), projection, whereClause,
                whereArgs, groupBy, having, sortOrder, limit);
    }

    /**
     * Query the given table, returning a Cursor over the result set.
     *
     * @param context
     * @param clazz
     * @param whereClauseMap A filter declaring which rows to return, formatted as an SQL
     *                       WHERE clause (excluding the WHERE itself). Passing null will
     *                       return all rows for the given table.
     * @param projection     A list of which columns to return. Passing null will return
     *                       all columns, which is discouraged to prevent reading data from
     *                       storage that isn't going to be used.
     * @param groupBy        A filter declaring how to group rows, formatted as an SQL
     *                       GROUP BY clause (excluding the GROUP BY itself). Passing null
     *                       will cause the rows to not be grouped.
     * @param having         formatted as an SQL HAVING clause (excluding the HAVING
     *                       itself). Passing null will cause all row groups to be
     *                       included, and is required when row grouping is not being used.
     * @param sortOrder      How to order the rows, formatted as an SQL ORDER BY clause
     *                       (excluding the ORDER BY itself). Passing null will use the
     *                       default_image sort order, which may be unordered.
     * @return A Cursor object, which is positioned before the first entry. Note
     * that Cursors are not synchronized, see the documentation for more
     * details.
     */
    public Cursor query(Context context, Class<?> clazz,
                        Map<String, String> whereClauseMap, String[] projection,
                        String groupBy, String having, String sortOrder) {
        String whereClause = getWhereClause(whereClauseMap);
        String[] whereArgs = getWhereArgs(whereClauseMap);
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        return db.query(getTableName(clazz), projection, whereClause,
                whereArgs, groupBy, having, sortOrder);
    }

    /**
     * Query the given table, returning a Cursor over the result set.
     *
     * @param context
     * @param clazz
     * @param rawQuery
     * @return A Cursor object, which is positioned before the first entry. Note
     * that Cursors are not synchronized, see the documentation for more
     * details.
     */
    public Cursor query(Context context, Class<?> clazz, String rawQuery) {
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        return db.rawQuery(rawQuery, null);
    }

    /**
     * Query the given table, returning a Cursor over the result set.
     *
     * @param context
     * @param clazz
     * @param queryBuilder   This is a convience class that helps build SQL queries to be
     *                       sent to SQLiteDatabase objects.
     * @param whereClauseMap A filter declaring which rows to return, formatted as an SQL
     *                       WHERE clause (excluding the WHERE itself). Passing null will
     *                       return all rows for the given table.
     * @param projection     A list of which columns to return. Passing null will return
     *                       all columns, which is discouraged to prevent reading data from
     *                       storage that isn't going to be used.
     * @param groupBy        A filter declaring how to group rows, formatted as an SQL
     *                       GROUP BY clause (excluding the GROUP BY itself). Passing null
     *                       will cause the rows to not be grouped.
     * @param having         formatted as an SQL HAVING clause (excluding the HAVING
     *                       itself). Passing null will cause all row groups to be
     *                       included, and is required when row grouping is not being used.
     * @param sortOrder      How to order the rows, formatted as an SQL ORDER BY clause
     *                       (excluding the ORDER BY itself). Passing null will use the
     *                       default_image sort order, which may be unordered.
     * @param limit          Limits the number of rows returned by the query, formatted as
     *                       LIMIT clause. Passing null denotes no LIMIT clause.
     * @return A Cursor object, which is positioned before the first entry. Note
     * that Cursors are not synchronized, see the documentation for more
     * details.
     */
    public Cursor query(Context context, Class<?> clazz,
                        SQLiteQueryBuilder queryBuilder,
                        Map<String, String> whereClauseMap, String[] projection,
                        String groupBy, String having, String sortOrder, String limit) {
        String whereClause = getWhereClause(whereClauseMap);
        String[] whereArgs = getWhereArgs(whereClauseMap);
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        return queryBuilder.query(db, projection, whereClause, whereArgs,
                groupBy, having, sortOrder, limit);
    }

    ;

    /**
     * Convenience method for deleting rows in the database.
     *
     * @param context
     * @param clazz
     * @param whereClauseMap the optional WHERE clause to apply when deleting. Passing null
     *                       will delete all rows.
     * @return the number of rows affected
     */
    public int delete(Context context, Class<?> clazz,
                      Map<String, String> whereClauseMap) {
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        String whereClause = getWhereClause(whereClauseMap);
        String[] whereArgs = getWhereArgs(whereClauseMap);
        return db.delete(getTableName(clazz), whereClause, whereArgs);
    }

    /**
     * Convenience method for deleting rows in the database.
     *
     * @param context
     * @param clazz
     * @param whereClause the optional WHERE clause to apply when deleting. Passing null
     *                    will delete all rows.
     * @return the number of rows affected
     */
    public int delete(Context context, Class<?> clazz, String whereClause) {
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        return db.delete(getTableName(clazz), whereClause, null);
    }

    public int delete(Class<? extends CommonsEntity> clss, Context context,
                      List<Long> idList) {
        String entityName = CommonsEntity.getEntityName(clss);
        String idColumnName = CommonsEntity.getIdColumnName(clss);
        String[] ids = idList.toArray(new String[idList.size()]);
        String inString = Collections.getCommaSeperatedStringFromArray(ids);
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        return db.delete(entityName, idColumnName + " IN (" + inString + ")",
                null);

    }

    public int deleteAll(Class<? extends CommonsEntity> clss, Context context) {
        String entityName = CommonsEntity.getEntityName(clss);
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        return db.delete(entityName, null, null);
    }

    public int delete(Class<?> clss, Context context, Criteria criteria, long id)
            throws DaoException, SQLException {
        String entityName = CommonsEntity.getEntityName(clss);
        String idColumnName = CommonsEntity.getIdColumnName(clss);
        List<String> whereArgs = new ArrayList<String>();

        String where = idColumnName + " = ?";
        if (criteria != null) {
            where = toCriteriaString(clss, criteria);
            if (Strings.isEmpty(where)) {
                where = "1=1";
            }
        }

        preparedStatementForDelete(clss, id, criteria, whereArgs);

        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        return db.delete(entityName, where,
                whereArgs.toArray(new String[whereArgs.size()]));

    }

    public void preparedStatementForDelete(Class<?> clss, long id,
                                           Criteria criteria, List<String> whereArgs) throws DaoException,
            SQLException {
        int i = 0;
        if (criteria == null) {
            whereArgs.add(i, "" + id);
        } else {
            populateCriteriaValues(whereArgs, clss, criteria, i);
        }
    }

    /**
     * @param whereClauseMap
     * @return where args for the given map
     */
    private String[] getWhereArgs(Map<String, String> whereClauseMap) {
        if (whereClauseMap == null) {
            return null;
        }
        String[] whereArgs = new String[whereClauseMap.size()];
        int count = 0;
        for (String key : whereClauseMap.keySet()) {
            whereArgs[count] = whereClauseMap.get(key);
            count++;
        }
        return whereArgs.length != 0 ? whereArgs : null;
    }

    /**
     * @param whereClauseMap
     * @return where clause for the given map
     */
    private String getWhereClause(Map<String, String> whereClauseMap) {
        if (whereClauseMap == null) {
            return null;
        }
        StringBuffer buffer = new StringBuffer();
        int count = 0;
        for (String key : whereClauseMap.keySet()) {
            buffer.append(key);
            buffer.append(" = ?");
            if (count < (whereClauseMap.size() - 1)) {
                buffer.append(" AND ");
            }
            count++;
        }
        return buffer.toString().length() != 0 ? buffer.toString() : null;
    }

    /**
     * @param object
     * @return a new ContentValues for a given object
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     */
    public ContentValues getContentValues(Object object)
            throws IllegalAccessException, IllegalArgumentException {
        ContentValues values = new ContentValues();
        Class<?> clazz = object.getClass();
        do {
            for (Field field : object.getClass().getDeclaredFields()) {
//				Annotation idAnnotation = field.getAnnotation(Id.class);
//				if (!isValidProperty(field) || idAnnotation != null) {
//					continue;
//				}
                if (!isValidProperty(field)) {
                    continue;
                }

                field.setAccessible(true);

                putNullOrValue(values, field.getName(),
                        field.get(object) != null ? field.get(object)
                                .toString() : null);
            }
        } while ((clazz = clazz.getSuperclass()) != null
                && clazz != CommonsEntity.class);
        return values;
    }

    private ContentValues getContentValues(Class<?> clss, CommonsEntity entity,
                                           List<String> columnNameList, String[] extras)
            throws IllegalAccessException, InvocationTargetException,
            NoSuchMethodException, NoSuchFieldException {
        ContentValues values = new ContentValues();
        for (String columnName : columnNameList) {
            Object value = Reflection.getProperty(entity, columnName);
            putNullOrValue(values, columnName, value != null ? value.toString()
                    : null);
        }

        for (String columnName : extras) {
            if (!CommonsEntity.isColumnExist(clss, columnName)) {
                Object value = Reflection.getProperty(entity, columnName);
                putNullOrValue(values, columnName,
                        value != null ? value.toString() : null);
            }
        }
        return values;
    }

    /**
     * Put a value into the given ContentValues map.
     *
     * @param cv
     * @param key
     * @param value
     */
    public void putNullOrValue(ContentValues cv, String key, String value) {
        if (value == null) {
            cv.putNull(key);
        } else {
            cv.put(key, value);
        }
    }

    /**
     * @param field
     * @return true if field is valid for creating as a column of a table
     */
    public boolean isValidProperty(Field field) {
        Annotation transientAnnotation = field.getAnnotation(Transient.class);
        // Annotation isColumnignoreAnnotation =
        // field.getAnnotation(Column.class);
        if (transientAnnotation != null) {// isColumnignoreAnnotation == null ||
            return false;
        }

        return true;
    }

    /**
     * @param clazz
     * @return table name for the given class
     */
    @SuppressLint("DefaultLocale")
    public String getTableName(Class<?> clazz) {
        return CommonsEntity.getEntityName(clazz);
    }

    /**
     * @param clazz
     * @return the create query for given Class.
     */
    public String getCreateQuery(Context context, Class<?> clazz) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("CREATE TABLE ");
        buffer.append(DBUtil.getInstance(context).getTableName(clazz));
        buffer.append(" (");
        do {
            for (Field field : clazz.getDeclaredFields()) {

                if (!DBUtil.getInstance(context).isValidProperty(field)) {
                    continue;
                }
                Annotation annotation = field.getAnnotation(Id.class);
                Annotation clientId = field.getAnnotation(ClientId.class);
                buffer.append(field.getName());
                buffer.append(" ");
                if (field.getType().getSimpleName().equals("int")
                        || field.getType().getSimpleName().equals("Integer")
                        || field.getType().getSimpleName().equals("long")
                        || field.getType().getSimpleName().equals("Long")) {
                    buffer.append("INTEGER");
                } else if (field.getType().getSimpleName().equals("double")
                        || field.getType().getSimpleName().equals("Double")) {
                    buffer.append("REAL");
                } else {
                    buffer.append("TEXT");
                }

                if (annotation != null) {
                    buffer.append(" PRIMARY KEY AUTOINCREMENT NOT NULL");
                }
                if (clientId != null) {
                    buffer.append(" PRIMARY KEY NOT NULL");
                }
                buffer.append(",");
            }
        } while ((clazz = clazz.getSuperclass()) != null
                && clazz != CommonsEntity.class);

        return buffer.substring(0, buffer.toString().length() - 1) + ")";
    }

    private String toCriterionStr(Class<?> clss, Criterion criterion) {
        if (criterion instanceof LogicalExpression) {
            return toCriterionStr(clss, (LogicalExpression) criterion);
        } else if (criterion instanceof PropertyExpression) {
            return toCriterionStr(clss, (PropertyExpression) criterion);
        } else if (criterion instanceof Junction) {
            return toCriterionStr(clss, (Junction) criterion);
        }

        return null;
    }

    private String toCriterionStr(Class<?> clss, LogicalExpression le) {
        String criterionStr = "";

        String op = le.getOp();
        String lhsStr = toCriterionStr(clss, le.getLhs());
        String rhsStr = toCriterionStr(clss, le.getRhs());

        if (op != null) {
            if (LogicalExpression.AND.equals(op)) {
                criterionStr += " " + lhsStr + " AND " + rhsStr + " ";
            } else if (LogicalExpression.OR.equals(op)) {
                criterionStr += " " + lhsStr + " OR " + rhsStr + " ";
            }
        }
        return criterionStr;
    }

    private String toCriterionStr(Class<?> clss, PropertyExpression pe) {
        String criterionStr = "";

        String op = pe.getOp();
        String propertyName = pe.getPropertyName();
        String otherPropertyName = pe.getOtherPropertyName();

        if (op != null) {
            if (PropertyExpression.IS_NULL.equals(op)) {
                criterionStr += " `" + propertyName + "` IS NULL ";
            } else if (PropertyExpression.IS_NOT_NULL.equals(op)) {
                criterionStr += " `" + propertyName + "` IS NOT NULL ";
            } else if (PropertyExpression.EQUAL.equals(op)) {
                if (Strings.isEmpty(propertyName))
                    propertyName = CommonsEntity.getIdColumnName(clss);
                if (!Strings.isEmpty(otherPropertyName)) {
                    criterionStr += " `" + propertyName + "` = `"
                            + otherPropertyName + "` ";
                } else {
                    criterionStr += " `" + propertyName + "` = ? ";
                }
            } else if (PropertyExpression.NOT_EQUAL.equals(op)) {
                if (!Strings.isEmpty(otherPropertyName)) {
                    criterionStr += " `" + propertyName + "` <> `"
                            + otherPropertyName + "` ";
                } else {
                    criterionStr += " `" + propertyName + "` <> ? ";
                }
            } else if (PropertyExpression.BETWEEN.equals(op)) {
                criterionStr += " `" + propertyName + "` BETWEEN ? AND ? ";
            } else if (PropertyExpression.GE.equals(op)) {
                if (!Strings.isEmpty(otherPropertyName)) {
                    criterionStr += " `" + propertyName + "` >= `"
                            + otherPropertyName + "` ";
                } else {
                    criterionStr += " `" + propertyName + "` >= ? ";
                }
            } else if (PropertyExpression.GT.equals(op)) {
                if (!Strings.isEmpty(otherPropertyName)) {
                    criterionStr += " `" + propertyName + "` > `"
                            + otherPropertyName + "` ";
                } else {
                    criterionStr += " `" + propertyName + "` > ? ";
                }
            } else if (PropertyExpression.LE.equals(op)) {
                if (!Strings.isEmpty(otherPropertyName)) {
                    criterionStr += " `" + propertyName + "` <= `"
                            + otherPropertyName + "` ";
                } else {
                    criterionStr += " `" + propertyName + "` <= ? ";
                }
            } else if (PropertyExpression.LT.equals(op)) {
                if (!Strings.isEmpty(otherPropertyName)) {
                    criterionStr += " `" + propertyName + "` < `"
                            + otherPropertyName + "` ";
                } else {
                    criterionStr += " `" + propertyName + "` < ? ";
                }
            } else if (PropertyExpression.LIKE.equals(op)) {
                criterionStr += " `" + propertyName + "` LIKE(?) ";
            } else if (PropertyExpression.IN.equals(op)) {
                criterionStr += " `" + propertyName + "` IN(";

                Object[] values = pe.getValues();
                for (int i = 0; i < values.length; i++) {
                    if (i > 0) {
                        criterionStr += ",?";
                    } else {
                        criterionStr += "?";
                    }
                }

                criterionStr += ") ";
            }
        }
        return criterionStr;
    }

    private String toCriterionStr(Class<?> clss, Junction j) {
        String criterionStr = "";

        String op = j.getOp();
        List<Criterion> criterions = j.getCriterions();

        if (op != null) {
            String con = "";
            if (Junction.CONJUNCTION.equals(op)) {
                con = "AND";
            } else if (Junction.DISJUNCTION.equals(op)) {
                con = "OR";
            }

            if (criterions.size() > 0) {
                criterionStr += " (";
                for (int i = 0; i < criterions.size(); i++) {
                    Criterion criterion = criterions.get(i);
                    if (i > 0) {
                        criterionStr += " " + con + " ";
                    }
                    criterionStr += toCriterionStr(clss, criterion);
                }
                criterionStr += ") ";
            }
        }

        return criterionStr;
    }

    private String toCriteriaString(Class<?> clss, Criteria criteria) {
        String criteriaStr = "";

        List<Criterion> criterions = criteria.getCriterions();
        if (criterions != null && criterions.size() > 0) {
            for (int i = 0; i < criterions.size(); i++) {
                Criterion criterion = criterions.get(i);
                if (i > 0) {
                    criteriaStr += " AND ";
                }
                criteriaStr += toCriterionStr(clss, criterion);
            }
        }

        List<String> groupBys = criteria.getGroupBys();
        if (groupBys != null && groupBys.size() > 0) {
            // criteriaStr += " GROUP BY ";
            criteriaStr += "";
            for (int i = 0; i < groupBys.size(); i++) {
                String groupBy = groupBys.get(i);
                if (i > 0) {
                    criteriaStr += ", ";
                }
                criteriaStr += "`" + groupBy + "`";
            }
            criteriaStr += " ";
        }

        List<Order> orderBys = criteria.getOrderBys();
        if (orderBys != null && orderBys.size() > 0) {
            criteriaStr += " ORDER BY ";
            for (int i = 0; i < orderBys.size(); i++) {
                Order order = orderBys.get(i);
                String orderFieldName = order.getPropertyName();
                if (i > 0) {
                    criteriaStr += ", ";
                }

                criteriaStr += "`" + orderFieldName + "`";

                if (!order.isAscending()) {
                    criteriaStr += " DESC";
                }
            }
            criteriaStr += " ";
        }

        Integer firstResult = criteria.getFirstResult();
        Integer maxResults = criteria.getMaxResults();
        if (firstResult != null && maxResults != null) {
            criteriaStr += " LIMIT " + firstResult.intValue() + ","
                    + maxResults.intValue() + " ";
        } else if (maxResults != null) {
            criteriaStr += " LIMIT " + maxResults.intValue() + " ";
        }

        return criteriaStr;
    }

    public List<Long> getLocalId(Class<? extends CommonsEntity> clss,
                                 Context context, List<Long> entities)
            throws InstantiationException, IllegalAccessException,
            InvocationTargetException, NoSuchMethodException,
            NoSuchFieldException {
        String entityName = CommonsEntity.getEntityName(clss);
        String idColumnName = CommonsEntity.getIdColumnName(clss);
        List<Long> clientIds = new ArrayList<Long>();
        List<String> columnNameList = new ArrayList<String>();
        columnNameList.add(idColumnName);

        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();

        Cursor cursor = db.query(entityName,
                columnNameList.toArray(new String[columnNameList.size()]),
                "id IN(" + Strings.toCSV(entities) + ")", null, null, null,
                null);

        while (cursor.moveToNext()) {
            clientIds
                    .add((Long) setValueToObject(cursor, columnNameList, clss));

        }
        return clientIds;
    }

}
