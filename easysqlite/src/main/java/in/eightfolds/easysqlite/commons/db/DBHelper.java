package in.eightfolds.easysqlite.commons.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.widget.Toast;

import java.util.List;

public class DBHelper extends SQLiteOpenHelper {

	private static DBHelper instance;
	private static List<Class<?>> schemaToCreate;
	private static String folder_name;
	private static String db_file_name;
	private static String db_version;
	private static boolean store_db_on_sdCard;
	private Context context;

	/**
	 * 
	 * @param context
	 * @return the DBHelper singleton instance.
	 */
	public static DBHelper getInstance(Context context,
									   List<Class<?>> listOfSchemaToCreate, String folderName,
									   String dbFileName, String dbVersion, boolean storeDBOnSDCard) {
		schemaToCreate = listOfSchemaToCreate;
		folder_name = folderName;
		db_file_name = dbFileName;
		db_version = dbVersion;
		store_db_on_sdCard = storeDBOnSDCard;
		if (instance == null) {
			instance = new DBHelper(context);
		}

		return instance;
	}

	public static DBHelper getInstance(Context context) {
		if (instance == null) {
			instance = new DBHelper(context);
		}

		return instance;
	}

	private DBHelper(Context context) {
		super(context, (store_db_on_sdCard ? Environment
				.getExternalStorageDirectory() + "/" + folder_name + "/" : "")
				+ db_file_name, null, Integer.parseInt(db_version));
		this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		if (schemaToCreate == null) {
			Toast.makeText(
					context,
					"Schema list is empty. First set the listOfSchemaToCreate of DBUtil",
					Toast.LENGTH_LONG).show();
			return;
		}
		for (Class<?> clazz : schemaToCreate) {
			db.execSQL(DBUtil.getInstance(context).getCreateQuery(context,
					clazz));
		}

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

	@Override
	public SQLiteDatabase getReadableDatabase() {
		SQLiteDatabase db = super.getReadableDatabase();
		db.execSQL("PRAGMA foreign_keys = ON");
		return db;
	}

	@Override
	public SQLiteDatabase getWritableDatabase() {
		SQLiteDatabase db = super.getWritableDatabase();
		db.execSQL("PRAGMA foreign_keys = ON");
		return db;
	}





}
