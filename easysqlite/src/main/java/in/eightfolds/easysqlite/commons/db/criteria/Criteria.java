package in.eightfolds.easysqlite.commons.db.criteria;

import java.util.List;

public interface Criteria {
	public Class<?> getEntity();

	// public List<String> getColumns();
	public List<Criterion> getCriterions();

	public List<String> getGroupBys();

	public List<Order> getOrderBys();

	public Integer getFirstResult();

	public Integer getMaxResults();

	public Criteria add(Criterion criterion);

	// public Criteria addColumn(String column);
	public Criteria addGroupBy(String propertyName);

	public Criteria addOrderBy(Order order);

	public Criteria setFirstResult(int startPosition);

	public Criteria setMaxResults(int maxResults);
}
