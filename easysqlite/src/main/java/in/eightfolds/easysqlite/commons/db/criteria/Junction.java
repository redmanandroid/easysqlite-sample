package in.eightfolds.easysqlite.commons.db.criteria;

import java.util.ArrayList;
import java.util.List;

public class Junction implements Criterion {
	public static final String CONJUNCTION = "conjunction";
	public static final String DISJUNCTION = "disjunction";
	
	private String op;
	private List<Criterion> criterions;

	protected Junction(String op){
		this.op = op;
		this.criterions = new ArrayList<Criterion>();
	}
	
	public Junction add(Criterion criterion){
		criterions.add(criterion);
		return this;
	}

	public String getOp() {
		return op;
	}

	public List<Criterion> getCriterions() {
		return criterions;
	}

}
