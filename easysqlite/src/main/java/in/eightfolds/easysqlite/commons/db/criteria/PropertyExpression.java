package in.eightfolds.easysqlite.commons.db.criteria;

import java.util.Collection;

public class PropertyExpression implements Criterion {
	public static final String IS_NULL = "is_null";
	public static final String IS_NOT_NULL = "is_not_null";
	public static final String EQUAL = "equal";
	public static final String NOT_EQUAL = "not_equal";
	public static final String BETWEEN = "between";
	public static final String GE = ">=";
	public static final String GT = ">";
	public static final String LE = "<=";
	public static final String LT = "<";
	public static final String LIKE = "like";
	public static final String IN = "in";
	
	private String propertyName;
	private String otherPropertyName;
	private Object value1;
	private Object value2;
	private Object[] values;
	private String op;
	
	protected PropertyExpression(String propertyName, String op) {
		this.propertyName = propertyName;
		this.op = op;
	}

	protected PropertyExpression(String propertyName, String otherPropertyName, String op) {
		this.propertyName = propertyName;
		this.otherPropertyName = otherPropertyName;
		this.op = op;
	}
	
	protected PropertyExpression(String propertyName, Object value, String op) {
		this.propertyName = propertyName;
		this.value1 = value;
		this.op = op;
	}
	
	protected PropertyExpression(String propertyName, Object value1, Object value2, String op) {
		this.propertyName = propertyName;
		this.value1 = value1;
		this.value2 = value2;
		this.op = op;
	}
	
	protected PropertyExpression(String propertyName, Collection<?> values, String op) {
		this.propertyName = propertyName;
		this.values = values == null ? null : values.toArray();
		this.op = op;
	}
	
	protected PropertyExpression(String propertyName, Object[] values, String op) {
		this.propertyName = propertyName;
		this.values = values;
		this.op = op;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public String getOtherPropertyName() {
		return otherPropertyName;
	}

	public Object getValue1() {
		return value1;
	}

	public Object getValue2() {
		return value2;
	}

	public Object[] getValues() {
		return values;
	}

	public String getOp() {
		return op;
	}

}
