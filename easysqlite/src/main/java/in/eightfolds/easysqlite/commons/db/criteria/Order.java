package in.eightfolds.easysqlite.commons.db.criteria;

public class Order {
	private String propertyName;
	private boolean ascending;

	public Order(String propertyName, boolean ascending) {
		this.propertyName = propertyName;
		this.ascending = ascending;
	}
	
	public static Order asc(String propertyName) {
		return new Order(propertyName, true);
	}
	
	public static Order desc(String propertyName) {
		return new Order(propertyName, false);
	}

	public String getPropertyName() {
		return propertyName;
	}

	public boolean isAscending() {
		return ascending;
	}
	
}

