package in.eightfolds.easysqlite.commons.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteQueryBuilder;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

import in.eightfolds.easysqlite.commons.db.bean.CommonsEntity;
import in.eightfolds.easysqlite.commons.db.criteria.Criteria;
import in.eightfolds.easysqlite.commons.db.exception.DaoException;


public interface EntityDao {
    public long add(CommonsEntity entity) throws DaoException,
            IllegalAccessException, IllegalArgumentException;

    public void add(List<? extends CommonsEntity> entities)
            throws DaoException, IllegalAccessException,
            IllegalArgumentException;

    public int modify(CommonsEntity entity) throws DaoException,
            IllegalAccessException, InvocationTargetException,
            NoSuchMethodException, NoSuchFieldException;

    public int modify(CommonsEntity entity, Criteria criteria)
            throws DaoException, IllegalAccessException,
            InvocationTargetException, NoSuchMethodException,
            NoSuchFieldException;

    public void modify(List<? extends CommonsEntity> entities)
            throws DaoException, IllegalAccessException,
            InvocationTargetException, NoSuchMethodException,
            NoSuchFieldException;

    public void modifyGivenColumn(CommonsEntity entity,
                                  List<String> columnList, boolean isServerSideId)
            throws DaoException, IllegalAccessException,
            InvocationTargetException, NoSuchMethodException,
            NoSuchFieldException;

    public int delete(long id) throws DaoException, SQLException;

    public int delete(Criteria criteria) throws DaoException, SQLException;

    public int delete(List<Long> ids) throws DaoException;

    public int deleteAll() throws DaoException, SQLException;

    public CommonsEntity selectById(long id)
            throws DaoException, SQLException, InstantiationException,
            IllegalAccessException, InvocationTargetException,
            NoSuchMethodException, NoSuchFieldException;

    public List<? extends CommonsEntity> selectAll() throws DaoException,
            InstantiationException, IllegalAccessException,
            InvocationTargetException, NoSuchMethodException, SQLException,
            NoSuchFieldException;

    public List<? extends CommonsEntity> select(Criteria criteria) throws DaoException, InstantiationException,
            IllegalAccessException, InvocationTargetException,
            NoSuchMethodException, SQLException, NoSuchFieldException;

    public List<? extends CommonsEntity> select(SQLiteQueryBuilder builder,
                                                List<String> columnNameList, String selection) throws DaoException,
            InstantiationException, IllegalAccessException,
            InvocationTargetException, NoSuchMethodException, SQLException,
            NoSuchFieldException;

    public List<? extends Object> selectAllIds(Criteria criteria)
            throws DaoException, InstantiationException,
            IllegalAccessException, InvocationTargetException,
            NoSuchMethodException, SQLException, NoSuchFieldException;

    public CommonsEntity selectOne(Criteria criteria) throws DaoException,
            InstantiationException, IllegalAccessException,
            InvocationTargetException, NoSuchMethodException, SQLException,
            NoSuchFieldException;

    public List<? extends CommonsEntity> rawQuery(String query) throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException, NoSuchFieldException;

    public Cursor getCursor(String query) throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException, NoSuchFieldException;

}
