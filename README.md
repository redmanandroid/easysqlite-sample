# README #


Before using this library make sure you disable instant run else project won't run
# Usage:

1.Include this  dependency inside your project

    compile 'in.eightfolds:easysqlite:1.0.0'

	
2.Create Application class 
   
     public class SqliteExampleApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        DBHelper.getInstance(getApplicationContext(), schemaToCreate(),
                getApplicationContext().getString(R.string.app_name),
                "My database",    //database name
                "1", false)      // database version
                .getWritableDatabase();
    }

    private List<Class<?>> schemaToCreate() {
        List<Class<?>> classes = new ArrayList<Class<?>>();
        classes.add(Student.class);                           //your classes you want to save
        classes.add(Language.class);

        return classes;
         }
    }

    add this application class name in androidmanifest.xml 

3.Add dependency  jackson annotation and databinding in build.gradle(app)
  
   
    compile 'com.fasterxml.jackson.core:jackson-annotations:2.5.2'
    
    if required 
    compile 'com.fasterxml.jackson.core:jackson-databind:2.5.2'

4.Your class should extend CommonsEntity 
  Your class should contain annotation  @Entity(name = "Language") 
   example class 
  
   
    @Entity(name = "Language")   //table name
    public class Language extends CommonsEntity{

    String studentid;
    String languageName;
    String languageid;

    public String getLanguageid() {
        return languageid;
    }

    public void setLanguageid(String languageid) {
        this.languageid = languageid;
    }

    public Language() {

    }

    public Language(String studentid, String languageName, String languageid) {
        this.studentid = studentid;
        this.languageName = languageName;
        this.languageid = languageid;
    }

    public String getStudentid() {
        return studentid;
    }

    public void setStudentid(String studentid) {
        this.studentid = studentid;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }
    }

5.DBManager.class

    public class DbManager {
    static Context context;
    private static DbManager dbManager;
    private static EntityDaoImpl studentDaoImpl;
    private static EntityDaoImpl languageDaoImpl;


    private DbManager() {

    }

    public static DbManager getInstance(Context context) {
        DbManager.context = context;
        if (dbManager == null) {
            dbManager = new DbManager();
            studentDaoImpl = new EntityDaoImpl(context, Student.class);
            languageDaoImpl = new EntityDaoImpl(context, Language.class);

        }
        return dbManager;
    }

    public void addStudentsList(List<Student> studentList) {
        for (Student student : studentList) {
            addStudent(student);
        }
    }


    public long addStudent(Student student) {
        try {
            Criteria criteria = CriteriaFactory.create(Student.class);
            criteria.add(Restrictions.eq("id", student.getId()));
            int count = studentDaoImpl.modify(student, criteria);
            if (count == 0) {
                return studentDaoImpl.add(student);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    public List<Student> getStudents() {
        try {
            return (List<Student>) studentDaoImpl.selectAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public int deleteStudents() {
        try {
            return studentDaoImpl.deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void addLanguageList(List<Language> languageList) {
        for (Language language : languageList) {
            addLanguage(language);
        }

    }


    public long addLanguage(Language language) {
        try {
            Criteria criteria = CriteriaFactory.create(Language.class);
            criteria.add(Restrictions.eq("languageid", language.getLanguageid()));
            int count = languageDaoImpl.modify(language, criteria);
            if (count == 0) {
                return languageDaoImpl.add(language);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    public int removeLanguage(String languageId) {
        try {
            Criteria criteria = CriteriaFactory.create(Language.class);
            criteria.add(Restrictions.eq("languageid", languageId));

            return languageDaoImpl.delete(criteria);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public boolean checkLanguage(String languageId) {
        try {
            Criteria criteria = CriteriaFactory.create(Language.class);
            criteria.add(Restrictions.eq("languageid", languageId));
            List<Language> languageList = (List<Language>) languageDaoImpl.select(criteria);

            if (languageList.size() != 0)
                return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public List<Language> getLanguageOfStudent(String studentId) {
        try {
            Criteria criteria = CriteriaFactory.create(Language.class);
            criteria.add(Restrictions.eq("studentid", studentId));


                return (List<Language>) languageDaoImpl.select(criteria);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    }
